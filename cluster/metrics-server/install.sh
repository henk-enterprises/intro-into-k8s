#!/bin/bash

helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
helm install metrics-server -n monitoring metrics-server/metrics-server
