from os import getenv

from flask import Flask, request, render_template, make_response, url_for, session, send_file, Response

import re

from flask_session import Session
from namespaceProvider import K8sClient
from werkzeug.utils import redirect

app = Flask(__name__)
app.config['SECRET_KEY'] = getenv('SECRET_KEY', 'ermegherd')
app.config['SESSION_TYPE'] = 'filesystem'
Session(app)


def gun_namespace(name):
    # Replace all non alphanumeric chars with "-"
    name = re.sub('[^0-9a-zA-Z]+', '-', name)
    client = K8sClient()
    return client.create_new_namespace(name)


def gun_kubectl_config(ns):
    client = K8sClient()
    return client.get_kubectl_data(ns)


@app.route("/")
def hi():
    return redirect(url_for('namespace'))


@app.route('/gun-namespace', methods=['GET', 'POST'])
def namespace():
    if request.method == 'POST':
        if 'namespace' in session:
            return render_template('namespace.html', message="Je hebt al een namespace, jij hebberd!", ns_name=session['namespace'])

        if 'name' in request.form and request.form['name'] is not None:
            ns = gun_namespace(request.form['name'])
            resp = make_response(render_template("namespace.html", message="Je namespace is aangemaakt!", ns_name=ns))
            session['namespace'] = ns

            return resp
        else:
            return render_template('form.html', error="Naam is niet ingevuld!")
    else:
        if 'namespace' not in session:
            return render_template('form.html')
        else:
            return render_template('namespace.html', ns_name=session['namespace'])
    pass


@app.route('/gun-config')
def get_config():
    if 'namespace' in session:
        data = gun_kubectl_config(session['namespace'])
        yaml = render_template('config.yaml', **data)
        return Response(yaml, mimetype='text/yaml', headers={"Content-disposition": "attachment; filename=config"})
    else:
        return render_template('form.html', error="Je hebt nog geen namespace aangemaakt!")


if __name__ == '__main__':
    app.run(debug=getenv('APP_ENV') != 'production', port=8000, host="0.0.0.0")
